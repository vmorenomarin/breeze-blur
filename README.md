A theme based in Breeze that invokes the blurred style.

This fork is based on the work Brisa Qtcurve 1.6 by alesandromarkes

Is required qtcurve-kf5 installed and you should have enabled the transparency and the blur in Desktop Effects. 

For installation, open System Settings > Widget Style > (Select QtCurve in list) > Configure > Import > (select the downloaded file) > Apply the changes.

Comments and observations: vmorenomarin@gmail.com
